package com.crosssoftware.erecepta.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.beans.AuthorizationBean;

@WebFilter("/pages/protected/*")
public class AuthFilter implements Filter {
	
	private static final Logger LOG = Logger.getLogger(AuthFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		LOG.info("Auth filter init.....");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        AuthorizationBean authorization = (AuthorizationBean) httpRequest.getSession().getAttribute("authorizationBean");

        if (authorization != null && authorization.isLogedIn()) {
            chain.doFilter(request, response);
        } else {
        	LOG.info("User is not logged, redirecting...");
            HttpServletResponse httpResponse = (HttpServletResponse) response;
			httpResponse.sendRedirect(httpRequest.getContextPath() + "/pages/public/login.xhtml");
        }
	}

	@Override
	public void destroy() {
		//empty...
	}
}