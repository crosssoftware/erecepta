package com.crosssoftware.erecepta.validators;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("valueComparationValidator")
public class ValueComparationValidator implements Validator {
	
	protected static final String CENA_DETALICZNA_KEY = "cenaDetValue";
	protected static final String WARTOSC_BLEDNA_DETAILS = "Cena po refundacji musi być mniejsza od ceny detalicznej.";
	protected static final String WARTOSC_BLEDNA_MSG = "Wartość błędna!";

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

		BigDecimal cenaDetaliczna = BigDecimal.ZERO;
		if (cenyNieSaPuste(component, value)) {
			cenaDetaliczna = (BigDecimal) component.getAttributes().get(CENA_DETALICZNA_KEY);

			if (cenaRefundacjiWiekszaOdDetalicznej((BigDecimal)value, cenaDetaliczna)) {
				FacesMessage msg = new FacesMessage(WARTOSC_BLEDNA_MSG,	WARTOSC_BLEDNA_DETAILS);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
			}
		}
	}

	private boolean cenyNieSaPuste(UIComponent component, Object value) {
		return (component.getAttributes().get(CENA_DETALICZNA_KEY) != null)	&& value != null;
	}

	private boolean cenaRefundacjiWiekszaOdDetalicznej(BigDecimal value, BigDecimal cenaDetaliczna) {
		return value.compareTo(cenaDetaliczna) > 0;
	}
}
