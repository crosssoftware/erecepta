package com.crosssoftware.erecepta.validators;


import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("kodUprawnienValidator")
public class KodUprawnienValidator implements Validator {
	
	protected static final String brakKodu = "Nieprawidlowy kod uprawnien";
	protected static final String blednyKod = "Nie istnieje podany kod uprawnień";
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

		String inwalidaWojenny = "IB";
		String inwalidaWojskowy = "IW";
		String zasluzonyKrwiodawca = "ZK";
		String brakUprawnien = "X";

		if (!((String) value).equals(inwalidaWojenny)
				&& !((String) value).equals(inwalidaWojskowy)
				&& !((String) value).equals(zasluzonyKrwiodawca)
				&& !((String) value).equals(brakUprawnien)) {

			FacesMessage msg = new FacesMessage(brakKodu, blednyKod);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
		}



	}
}