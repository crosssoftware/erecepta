package com.crosssoftware.erecepta.validators;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("kodPocztowyValidator")
public class KodPocztowyValidator implements Validator {

	protected static final String blednyFormat = "Format kodu pocztowego ma postać nn-nnn";

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		ArrayList<String> lista = new ArrayList<String>(); // warotsci 0-9
		ArrayList<String> valueLista = new ArrayList<String>(); // lista
																// wprowadzonych
																// znakow
		String temp = new String();
		temp = value.toString();

		for (int i = 0; i < temp.length(); i++) {
			valueLista.add(i, String.valueOf(temp.charAt(i)));
		}


		for (int i = 0; i < 10; i++) {
			lista.add(i, Integer.toString(i));
		}
		lista.add("-");

		for (int i = 0; i < temp.length(); i++) {
			if (i == 2) {
				if (!valueLista.get(i).equals("-")) {
					FacesMessage msg = new FacesMessage(blednyFormat);
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					throw new ValidatorException(msg);
				}
			} else if (!lista.contains((String) valueLista.get(i))) {
				FacesMessage msg = new FacesMessage(blednyFormat);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
			}
		}

	}

}
