package com.crosssoftware.erecepta.validators;


import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("kodnfzValidator")
public class KodnfzValidator implements Validator {

	protected static final String blednyKod = "Kod odzzialu NFZ jest dwuznakowym ciągiem cyfr od 01 - 16";
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		ArrayList<String> lista = new ArrayList<String>();

		for (int i = 0, val1 = 1, val2 = 1; i < 16; i++, val1++, val2++) {
			if (i < 10)
				lista.add(i, "0" + Integer.toString(val2));
			else {
				lista.add(i, Integer.toString(val1));
			}
		}
		lista.add(10, "10");

		if (!lista.contains((String) value)) {
			FacesMessage msg = new FacesMessage(blednyKod);
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}