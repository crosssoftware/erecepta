package com.crosssoftware.erecepta.validators;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("peselValidator")
public class PeselValidator implements Validator {

	protected static final String blednyPesel = "Pesel składa się z 11 cyfr";

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		ArrayList<String> lista = new ArrayList<String>();
		ArrayList<String> valueLista = new ArrayList<String>();
		String temp = new String();
		temp = value.toString();

		for (int i = 0; i < temp.length(); i++) {
			valueLista.add(i, String.valueOf(temp.charAt(i)));
		}

		for (int i = 0; i < 10; i++) {
			lista.add(i, Integer.toString(i) );
		}

		for (int i = 0; i < temp.length(); i++) {
			if (!lista.contains((String) valueLista.get(i))) {
				FacesMessage msg = new FacesMessage(blednyPesel);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
			}
		}

	}
}
