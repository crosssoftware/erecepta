package com.crosssoftware.erecepta.model.dao;

import javax.ejb.Local;

import com.crosssoftware.erecepta.model.ds.Recepta;
import com.crosssoftware.erecepta.model.ds.Zoz;

@Local
public interface ZozDao {

	Zoz getZoz(Recepta recepta);
}
