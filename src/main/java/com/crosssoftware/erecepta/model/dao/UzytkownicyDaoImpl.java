package com.crosssoftware.erecepta.model.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.ds.Uzytkownik;

/**
 * @author Emil
 */
@Stateless
@LocalBean
public class UzytkownicyDaoImpl implements UzytkownicyDao {

	@PersistenceContext(name = "EreceptaPU")
	private EntityManager em;

	private static final Logger LOG = Logger.getLogger(UzytkownicyDaoImpl.class);

	@Override
	public boolean czyIstnieje(Uzytkownik ds) {
		TypedQuery<Integer> q = em
				.createQuery(
						"Select COUNT(*) FROM UsersDs u Where u.nazwa=:nazwa AND u.haslo=:haslo",
						Integer.class);
		q.setParameter("nazwa", ds.getLogin());
		q.setParameter("haslo", ds.getHaslo());

		return q.getSingleResult() > 0;
	}

	@Override
	public void dodajUsera(Uzytkownik ds) {
		LOG.info("Dodano usera do bazy");
		em.persist(ds);
	}
	
	@Override
	public boolean czyPoprawnyLogin(Uzytkownik user) {
		TypedQuery<Long> query = em.createQuery("SELECT COUNT (*) FROM Uzytkownik c WHERE c.login = :login AND c.haslo = :haslo", Long.class);
		query.setParameter("login", user.getLogin());
		query.setParameter("haslo", user.getHaslo());
		return query.getSingleResult() > 0;
	}

	@Override
	public Uzytkownik getUser(String login) {
		TypedQuery<Uzytkownik> query = em.createQuery("SELECT c FROM Uzytkownik c WHERE c.login = :login", Uzytkownik.class);
		query.setParameter("login", login);
		Uzytkownik uzytkownik =  query.getSingleResult();
		//uzytkownik.getPacjents();
		return uzytkownik;
	}
}
