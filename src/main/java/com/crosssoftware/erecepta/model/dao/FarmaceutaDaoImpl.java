package com.crosssoftware.erecepta.model.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.ds.Farmaceuta;

@Stateless
public class FarmaceutaDaoImpl implements FarmaceutaDao {

	@PersistenceContext(name = "EreceptaPU")
	private EntityManager em;
	
	private static final Logger LOG = Logger.getLogger(LekarzDaoImpl.class);
	
	@Override
	public Farmaceuta getByIdUzytkownika(long id) {
		LOG.info(String.format("Pobieranie lekarza z id uzytkownika: %d z bazy", id));
		TypedQuery<Farmaceuta> query = em.createQuery(
				"SELECT f FROM Farmaceuta f where f.uzytkownik.iduzytkownika = :id", Farmaceuta.class);
		query.setParameter("id", (int)id);
		
		List<Farmaceuta> results = query.getResultList();
        if (!results.isEmpty()) {
        	return results.get(0);
        } else {
        	return null;
        }
	}

}
