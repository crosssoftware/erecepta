package com.crosssoftware.erecepta.model.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.crosssoftware.erecepta.model.ds.Postacleku;

@Stateless
public class PostacLekuDaoImpl implements PostacLekuDao {

	@PersistenceContext(name = "EreceptaPU")
	private EntityManager em;

	@Override
	public List<Postacleku> getAllPostacLeku() {
		TypedQuery<Postacleku> query = em.createQuery("SELECT l FROM Postacleku l", Postacleku.class);
		return query.getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Postacleku getPostacLekuByNazwa(String nazwapostacileku) {
		TypedQuery<Postacleku> query = em.createQuery(
				"SELECT p FROM Postacleku p WHERE p.nazwapostacileku = :nazwa",
				Postacleku.class);
		query.setParameter("nazwa", nazwapostacileku);
		return query.getSingleResult();
	}

	@Override
	public Postacleku getPostacLekuById(int idPostaciLeku) {
		TypedQuery<Postacleku> query = em.createQuery(
				"SELECT l FROM Postacleku l WHERE l.idpostacileku = :id", Postacleku.class);
		query.setParameter("id", idPostaciLeku);
		return query.getSingleResult();
	}

}
