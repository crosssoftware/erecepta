package com.crosssoftware.erecepta.model.dao;

import java.util.List;

import com.crosssoftware.erecepta.model.ds.Postacleku;

public interface PostacLekuDao {
	List<Postacleku> getAllPostacLeku();
	
	Postacleku getPostacLekuById(int idPostaciLeku);

	Postacleku getPostacLekuByNazwa(String nazwaPostaci);
}
