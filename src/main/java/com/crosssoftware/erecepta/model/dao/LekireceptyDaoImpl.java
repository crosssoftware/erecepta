package com.crosssoftware.erecepta.model.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.Recepta;

@Stateless
public class LekireceptyDaoImpl implements LekireceptyDao {

	@PersistenceContext(name = "EreceptaPU")
	private EntityManager em;
	
	private static final Logger LOG = Logger.getLogger(LekireceptyDao.class);
	
	@Override
	public void save(Lekirecepty lek) {
		LOG.info("Zapis leku recepty do DB...");
		em.merge(lek);
	}

	@Override
	public List<Lekirecepty> getAllByRecepta(Recepta recepta) {
		TypedQuery<Lekirecepty> query = em.createQuery(
				"SELECT l FROM Lekirecepty l JOIN FETCH l.recepta WHERE l.recepta = :recepta", Lekirecepty.class);
		query.setParameter("recepta", recepta);
		return query.getResultList();
	}
	
	@Override
	public List<Lekirecepty> getAllLeksRecept(Recepta recepta) {
		LOG.info("Odczyt lekow z DB...");
		TypedQuery<Lekirecepty> query = em.createQuery("SELECT lr FROM Lekirecepty lr JOIN FETCH lr.lek  WHERE lr.recepta = :recepta",Lekirecepty.class);
		query.setParameter("recepta", recepta);
		LOG.info(query.getResultList());
		return query.getResultList();
	}
	
	@Override
	public void update(Lekirecepty thisLek) {
		LOG.info("Edycja leku w DB...");
		em.merge(thisLek);
	}

	@Override
	public long countLiczbaLekow(Recepta recepta) {
		TypedQuery<Long> query = em.createQuery("SELECT COUNT(*) FROM Lekirecepty lr WHERE lr.recepta = :recepta", Long.class);
		query.setParameter("recepta", recepta);
		return query.getSingleResult();
	}
}
