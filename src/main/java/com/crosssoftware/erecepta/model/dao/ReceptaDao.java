package com.crosssoftware.erecepta.model.dao;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.Pacjent;
import com.crosssoftware.erecepta.model.ds.Recepta;

/**
 * @author pawegio
 */
@Local
public interface ReceptaDao {

	void save(Recepta recepta, List<Lekirecepty> leki);
	
	List<Recepta> getAllRecepty(Pacjent pacjent);
	
	long getLiczbaLekow(Recepta recepta);

	long count();

	void zrealizuj(long numerRecepty, Date date);
}
