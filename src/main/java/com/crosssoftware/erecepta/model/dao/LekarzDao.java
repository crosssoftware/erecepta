package com.crosssoftware.erecepta.model.dao;

import com.crosssoftware.erecepta.model.ds.Lekarz;

public interface LekarzDao {

	Lekarz getById(long id);
	
	String getZozName(Lekarz lekarz);
}
