package com.crosssoftware.erecepta.model.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.ds.Lek;
import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.Postacleku;

/**
 * @author mgorgon
 */
@Stateless
public class LekDaoImpl implements LekDao {

	@PersistenceContext(name = "EreceptaPU")
	private EntityManager em;

	@EJB
	private PostacLekuDao postacLekuDao;

	private static final Logger LOG = Logger.getLogger(LekDaoImpl.class);

	@Override
	public void save(Lek lek) {
		LOG.info("Zapis leku do DB...");
		em.persist(lek);
	}

	@Override
	public void update(Lek thisLek) {
		LOG.info("Edycja leku w DB...");
		em.merge(thisLek);
	}

	@Override
	public List<Lek> getAllLeks(String nazwa) {
		LOG.info("Odczyt lekow z DB...");
		TypedQuery<Lek> query = em
				.createQuery(
						"SELECT l FROM Lek l WHERE l.nazwaleku = :nazwaleku",
						Lek.class);
		query.setParameter("nazwaleku", nazwa);
		return query.getResultList();
	}

	@Override
	public boolean doesExist(Lek thisLek) {
		LOG.info("Odczyt lekow z DB...");
		TypedQuery<Long> query = em
				.createQuery(
						"SELECT COUNT(*) FROM Lek l where l.nazwaleku = :nazwaleku AND l.wielkoscopakowania = :wielkoscopakowania"
								+ " AND l.dawkasubstancjiczynnej = :dawkasubstancjiczynnej AND l.idpostacileku = :idpostacileku "
								+ "AND l.sklad = :sklad", Long.class);

		query.setParameter("nazwaleku", thisLek.getNazwaleku());
		query.setParameter("wielkoscopakowania",
				thisLek.getWielkoscopakowania());
		query.setParameter("dawkasubstancjiczynnej",
				thisLek.getDawkasubstancjiczynnej());
		query.setParameter("idpostacileku", thisLek.getIdpostacileku());
		query.setParameter("sklad", thisLek.getSklad());
		if (query.getSingleResult() > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean doesExistWithoutThis(Lek selectedLek) {
		TypedQuery<Integer> query = em.createQuery("SELECT l.idleku FROM Lek l where l.nazwaleku = :nazwaleku AND l.wielkoscopakowania = :wielkoscopakowania"
				+ " AND l.dawkasubstancjiczynnej = :dawkasubstancjiczynnej AND l.idpostacileku = :idpostacileku "
				+ "AND l.sklad = :sklad", Integer.class);
		
		query.setParameter("nazwaleku", selectedLek.getNazwaleku());
		query.setParameter("wielkoscopakowania",
				selectedLek.getWielkoscopakowania());
		query.setParameter("dawkasubstancjiczynnej",
				selectedLek.getDawkasubstancjiczynnej());
		query.setParameter("idpostacileku", selectedLek.getIdpostacileku());
		query.setParameter("sklad", selectedLek.getSklad());
		
		try {
			if (query.getSingleResult().intValue() == selectedLek.getIdleku()) {
				return false;
			} else {
				return true;
			}
		} catch (NoResultException e) {
			return false;
		}
	}

	@Override
	public List<String> pobierzLekiRozpoczynajaceSieOd(String nazwa) {
		LOG.info("Odczyt lekow z DB...");
		TypedQuery<Lek> query = em
				.createQuery(
						"SELECT l FROM Lek l WHERE l.nazwaleku LIKE :nazwaleku",
						Lek.class);
		query.setParameter("nazwaleku", nazwa + "%");
		List<String> nazwy = new ArrayList<>();
		for (Lek lek : query.getResultList()) {
			nazwy.add(lek.getNazwaleku());
		}
		return nazwy;		
	}

	@Override
	public List<Lek> pobierzLeki(String nazwa, int idPostaciLeku) {
		LOG.info("Odczyt leku z DB...");
		TypedQuery<Lek> query = em
				.createQuery(
						"SELECT l FROM Lek l WHERE l.nazwaleku = :nazwaleku"
						+ " AND l.idpostacileku = :idpostacileku",
						Lek.class);
		query.setParameter("nazwaleku", nazwa);
		query.setParameter("idpostacileku", idPostaciLeku);
		return query.getResultList();
	}

	@Override
	public Lek pobierzLek(Lekirecepty lek) {
		LOG.info("Odczyt leku z DB...");
		TypedQuery<Lek> query = em
				.createQuery(
						"SELECT l.lek FROM Lekirecepty l JOIN l.lek WHERE l = :lek",
						Lek.class);
		query.setParameter("lek", lek);
		return query.getSingleResult();
	}

	@Override
	public Lek pobierzLek(String nazwa, int idPostaciLeku,
			String wielkoscOpakowania) {
		LOG.info("Odczyt leku z DB...");
		TypedQuery<Lek> query = em
				.createQuery(
						"SELECT l FROM Lek l WHERE l.nazwaleku = :nazwaleku"
						+ " AND l.idpostacileku = :idpostacileku"
						+ " AND l.wielkoscopakowania = :wielkoscopakowania",
						Lek.class);
		query.setParameter("nazwaleku", nazwa);
		query.setParameter("idpostacileku", idPostaciLeku);
		query.setParameter("wielkoscopakowania", wielkoscOpakowania);
		return query.getSingleResult();
	}

	@Override
	public String getPostacLeku(int x) {
		LOG.info("Pobieranie postaci leku id " + x);
		TypedQuery<Postacleku> query = em
				.createQuery(
						"SELECT p FROM Postacleku p WHERE p.idpostacileku = :postac",
						Postacleku.class);
		query.setParameter("postac", x);
		String postac =  query.getSingleResult().getNazwapostacileku();
		return postac;
	}

}
