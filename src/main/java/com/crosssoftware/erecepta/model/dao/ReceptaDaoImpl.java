package com.crosssoftware.erecepta.model.dao;

import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.LekireceptyId;
import com.crosssoftware.erecepta.model.ds.Pacjent;
import com.crosssoftware.erecepta.model.ds.Recepta;

@Stateless
@LocalBean
public class ReceptaDaoImpl implements ReceptaDao {

	@PersistenceContext(name = "EreceptaPU")
	private EntityManager em;

	private static final Logger LOG = Logger.getLogger(ReceptaDaoImpl.class);

	
	@Override
	public List<Recepta> getAllRecepty(Pacjent pacjent) {
		LOG.info("Odczyt recept z DB...");
		TypedQuery<Recepta> query = em
				.createQuery("SELECT r FROM Recepta r JOIN FETCH r.pacjent JOIN FETCH r.lekarz"
						    + " WHERE r.pacjent = :pacjent",
						Recepta.class);
		query.setParameter("pacjent", pacjent);
		
		
		List<Recepta> result = query.getResultList();
		for (Recepta recepta : result) {
			recepta.getLekirecepties();
		}
		
		return result;
		
//		 LOG.info("Odczyt recept z DB...");
//		TypedQuery<Recepta> query = em.createQuery("SELECT r FROM Recepta r  WHERE r.pacjent = :pacjent",Recepta.class);
//		query.setParameter("pacjent", pacjent);
//
//		List<Recepta> recepty = query.getResultList();
//		for(Recepta r : recepty) {
//  		r.getPacjent();
//		r.getLekarz();
//		r.getLekarz().getZoz();
//		}
//		return recepty;
		
	}
	
	public long count() {
		Query query = em.createQuery(
		        "SELECT COUNT(*) FROM Recepta");
		return (long) query.getSingleResult();
	}

	@Override
	public long getLiczbaLekow(Recepta recepta) {
		LOG.info("Odczyt recept z DB...");
		TypedQuery<Long> query = em
				.createQuery("SELECT COUNT(*) FROM Lekirecepty l JOIN l.recepta"
						    + " WHERE l.recepta = :recepta",
						Long.class);
		query.setParameter("recepta", recepta);
		return query.getSingleResult();
	}

	@Override
	public void save(Recepta recepta, List<Lekirecepty> leki) {
		em.persist(recepta);
		for (Lekirecepty lekirecepty : leki) {
			lekirecepty.setId(new LekireceptyId(lekirecepty.getLek().getIdleku(), recepta.getNumerrecepty()));
			lekirecepty.setRecepta(recepta);
			em.merge(lekirecepty);
		}
	}

	@Override
	public void zrealizuj(long numerRecepty, Date date) {
		Query q = em.createQuery("Update Recepta r set r.dataRealizacji = :date where r.numerrecepty = :numerRecepty");
		q.setParameter("numerRecepty", numerRecepty);
		q.setParameter("date", date);
		q.executeUpdate();
	}
}
