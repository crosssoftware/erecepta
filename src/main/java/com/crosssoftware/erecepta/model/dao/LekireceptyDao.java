package com.crosssoftware.erecepta.model.dao;

import java.util.List;

import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.Recepta;

public interface LekireceptyDao {

	void save(Lekirecepty lek);
	
	List<Lekirecepty> getAllByRecepta(Recepta recepta);
	List<Lekirecepty> getAllLeksRecept(Recepta recepta);
	void update(Lekirecepty thisLek);

	long countLiczbaLekow(Recepta recepta);
}
