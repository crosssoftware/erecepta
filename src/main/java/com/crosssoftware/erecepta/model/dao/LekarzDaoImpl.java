package com.crosssoftware.erecepta.model.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.ds.Lekarz;

@Stateless
public class LekarzDaoImpl implements LekarzDao {

	@PersistenceContext(name = "EreceptaPU")
	private EntityManager em;
	
	private static final Logger LOG = Logger.getLogger(LekarzDaoImpl.class);
	
	@Override
	public Lekarz getById(long id) {
		LOG.info(String.format("Pobieranie lekarza z id uzytkownika: %d z bazy", id));
		TypedQuery<Lekarz> query = em.createQuery(
				"SELECT l FROM Lekarz l where l.uzytkownik.iduzytkownika = :id", Lekarz.class);
		query.setParameter("id", (int)id);
		
		List<Lekarz> results = query.getResultList();
        if (!results.isEmpty()) {
        	return results.get(0);
        } else {
        	return null;
        }
	}

	@Override
	public String getZozName(Lekarz lekarz) {
		TypedQuery<String> query = em.createQuery(
				"SELECT l.zoz.nazwazoz FROM Lekarz l where l = :lekarz", String.class);
		query.setParameter("lekarz", lekarz);
		return query.getSingleResult();
	}

}
