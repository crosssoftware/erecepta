package com.crosssoftware.erecepta.model.dao;

import javax.ejb.Local;

import com.crosssoftware.erecepta.model.ds.Uzytkownik;

/**
 * @author Emil
 */
@Local
public interface UzytkownicyDao {
	
	boolean czyIstnieje(Uzytkownik ds);

	void dodajUsera(Uzytkownik ds);

	boolean czyPoprawnyLogin(Uzytkownik user);

	Uzytkownik getUser(String login);

}
