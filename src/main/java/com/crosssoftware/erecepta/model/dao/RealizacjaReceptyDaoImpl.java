package com.crosssoftware.erecepta.model.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.beans.AuthorizationBean;
import com.crosssoftware.erecepta.bo.FarmaceutaBo;
import com.crosssoftware.erecepta.model.ds.Farmaceuta;
import com.crosssoftware.erecepta.model.ds.Realizacjarecepty;
import com.crosssoftware.erecepta.model.ds.Recepta;

@Stateless
@LocalBean
public class RealizacjaReceptyDaoImpl implements RealizacjaReceptyDao {

	@PersistenceContext(name = "EreceptaPU")
	private EntityManager em;

	private static final Logger LOG = Logger.getLogger(LekDaoImpl.class);

	@Override
	public Realizacjarecepty getRealizacjaRecepty(Recepta recepta) {
		LOG.info("Odczyt Realizacji recepty z DB...");
		TypedQuery<Realizacjarecepty> query = em.createQuery(
				"SELECT r FROM Realizacjarecepty r "
						+ " WHERE r.recepta = :recepta ",
				Realizacjarecepty.class);
		query.setParameter("recepta", recepta);

		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public void update(Realizacjarecepty recepta) {
		LOG.info("Edycja leku w DB...");
		em.merge(recepta);
	}

	@Override
	public void zapisz(Realizacjarecepty realizacja) {
//		TypedQuery<Farmaceuta> query = em.createQuery("select f from farmaceuta where f.idfarmaceuty = 1", Farmaceuta.class);
//		Farmaceuta farma = query.getSingleResult();
//		
//		realizacja.setFarmaceuta(farma);
		LOG.info("Edycja realizacji recepty w DB...");
		em.merge(realizacja);
	}

}
