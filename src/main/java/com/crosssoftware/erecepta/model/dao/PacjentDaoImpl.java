package com.crosssoftware.erecepta.model.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.ds.Pacjent;

@Stateless
public class PacjentDaoImpl implements PacjentDao {

	@PersistenceContext(name = "EreceptaPU")
	private EntityManager em;

	private static final Logger LOG = Logger.getLogger(PacjentDaoImpl.class);

	@Override
	public void save(Pacjent pacjent) {
		LOG.info("Zapis pacjenta do DB...");
	
		TypedQuery<Long> query = em.createQuery("SELECT COUNT(*) FROM Pacjent",
				Long.class);

		pacjent.setIdpacjenta(query.getSingleResult().intValue() + 1);
		em.persist(pacjent);
	}

	@Override
	public void update(Pacjent thisPacjent) {
		LOG.info("Edycja pacjenta w DB...");
		em.merge(thisPacjent);
	}

	@Override
	public List<Pacjent> getAllPacjenci(String pesel, String imie,
			String nazwisko) {
		LOG.info("Odczyt pacjentow z DB...");
		TypedQuery<Pacjent> query = null;
		if (pesel != "" && nazwisko == "" && imie == "") {
			query = em.createQuery(
					"SELECT p FROM Pacjent p WHERE p.pesel = :pesel ",
				Pacjent.class);
		query.setParameter("pesel", pesel);

		return query.getResultList();
		} else if (pesel == "" && imie != "" && nazwisko == "") {
			query = em.createQuery(
					"SELECT p FROM Pacjent p WHERE p.imie = :imie ",
					Pacjent.class);
			query.setParameter("imie", imie);
			return query.getResultList();

		} else if (pesel == "" && imie == "" && nazwisko != "") {// !!
			query = em.createQuery(
					"SELECT p FROM Pacjent p WHERE p.nazwisko = :nazwisko ",
					Pacjent.class);
			query.setParameter("nazwisko", nazwisko);
			return query.getResultList();
		} else if (pesel == "" && imie != "" && nazwisko != "") {
			query = em
					.createQuery(
							"SELECT p FROM Pacjent p WHERE p.imie = :imie AND p.nazwisko = nazwisko ",
							Pacjent.class);
			query.setParameter("imie", imie);
			// query.setParameter("nazwisko", nazwisko);
			return query.getResultList();
		} else if (pesel != "" && imie != "" && nazwisko == "") {
			query = em
					.createQuery(
							"SELECT p FROM Pacjent p WHERE p.pesel = :pesel AND p.imie = imie ",
							Pacjent.class);

			query.setParameter("pesel", pesel);
			return query.getResultList();
		} else if (pesel != "" && imie == "" && nazwisko != "") {
			query = em
					.createQuery(
							"SELECT p FROM Pacjent p WHERE p.pesel = :pesel AND p.nazwisko = nazwisko ",
							Pacjent.class);
			query.setParameter("pesel", pesel);
			return query.getResultList();
		} else if (pesel != "" && imie != "" && nazwisko != "") {
			query = em
					.createQuery(
							"SELECT p FROM Pacjent p WHERE p.pesel = :pesel AND p.nazwisko = nazwisko AND p.imie = imie",
							Pacjent.class);
			query.setParameter("pesel", pesel);
			return query.getResultList();
		}

		return query.getResultList();
	}

	@Override
	public boolean doesExist(Pacjent thisPacjent) {
		LOG.info("Odczyt lekow z DB...");
		TypedQuery<Long> query = em.createQuery(
				"SELECT COUNT(*) FROM Pacjent p where p.pesel = :pesel",
				Long.class);

		query.setParameter("pesel", thisPacjent.getPesel());

		if (query.getSingleResult() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean doesExistWithoutThis(Pacjent selectedPacjent) {
		TypedQuery<String> query = em.createQuery(
				"SELECT p.pesel FROM Pacjent p where p.pesel = :pesel",
				String.class);

		query.setParameter("pesel", selectedPacjent.getPesel());
		try {
			if (query.getSingleResult().contains(selectedPacjent.getPesel())) {
				return false;
			} else {
				return true;
			}
		} catch (NoResultException e) {
			return false;
		}
	};
	
	@Override
	public boolean doesExist(String pesel) {
		TypedQuery<Long> query = em.createQuery(
				"SELECT COUNT(*) FROM Pacjent p where p.pesel = :pesel", Long.class);
		query.setParameter("pesel", pesel);

		if (query.getSingleResult() > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Pacjent getByPesel(String pesel) {
		TypedQuery<Pacjent> query = em.createQuery(
				"SELECT p FROM Pacjent p where p.pesel = :pesel", Pacjent.class);
		query.setParameter("pesel", pesel);
		return query.getSingleResult();
	}

	@Override
	public Pacjent getByIdUzytkownika(int idUzytkownika) {
		LOG.info("Pobieranie pacjenta o id uzytkownika: " + idUzytkownika);
		TypedQuery<Pacjent> query = em.createQuery(
				"SELECT p FROM Pacjent p where p.uzytkownik.iduzytkownika = :id", Pacjent.class);
		query.setParameter("id", idUzytkownika);

		List<Pacjent> results = query.getResultList();
        if (!results.isEmpty()) {
        	return results.get(0);
        } else {
        	return null;
        }
	}

	@Override
	public List<Pacjent> pobierzWszystkich() {
		LOG.info("Odczyt pacjentow z DB...");
		TypedQuery<Pacjent> query = em.createQuery("SELECT p FROM Pacjent p",Pacjent.class);
		return query.getResultList();
	}

	@Override
	public List<Pacjent> getAllPacjenci(String nazwa) {
		LOG.info("Odczyt pacjentow z DB...");
		TypedQuery<Pacjent> query = em
				.createQuery("SELECT p FROM Pacjent p WHERE p.pesel = :pesel",
						Pacjent.class);
		query.setParameter("pesel", nazwa);
		return query.getResultList();
	}

}
