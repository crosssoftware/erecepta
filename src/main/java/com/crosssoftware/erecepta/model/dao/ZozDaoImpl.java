package com.crosssoftware.erecepta.model.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.ds.Recepta;
import com.crosssoftware.erecepta.model.ds.Zoz;

@Stateless
@LocalBean
public class ZozDaoImpl implements ZozDao {

	@PersistenceContext(name = "EreceptaPU")
	private EntityManager em;

	private static final Logger LOG = Logger.getLogger(ZozDaoImpl.class);
	
	@Override
	public Zoz getZoz(Recepta recepta) {
		LOG.info("Odczyt ZOZ z DB...");
		TypedQuery<Zoz> query = em
				.createQuery("SELECT r.lekarz.zoz FROM Recepta r JOIN r.lekarz JOIN r.lekarz.zoz"
						    + " WHERE r = :recepta",
						Zoz.class);
		query.setParameter("recepta", recepta);
		return query.getSingleResult();
	}

}
