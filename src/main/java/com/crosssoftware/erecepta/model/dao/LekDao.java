package com.crosssoftware.erecepta.model.dao;

import java.util.List;

import com.crosssoftware.erecepta.model.ds.Lek;
import com.crosssoftware.erecepta.model.ds.Lekirecepty;

/**
 * @author mgorgon
 */
public interface LekDao {

	void save(Lek lek);
	
	void update(Lek thisLek);

	List<Lek> getAllLeks(String nazwa);

	boolean doesExist(Lek thisLek);
	
	boolean doesExistWithoutThis(Lek selectedLek);
	
	List<String> pobierzLekiRozpoczynajaceSieOd(String nazwa);
	
	List<Lek> pobierzLeki(String nazwa, int idPostaciLeku);
	
	Lek pobierzLek(Lekirecepty lek);
	
	Lek pobierzLek(String nazwa, int idPostaciLeku, String wielkoscOpakowania);
	


	String getPostacLeku(int x);
 }
