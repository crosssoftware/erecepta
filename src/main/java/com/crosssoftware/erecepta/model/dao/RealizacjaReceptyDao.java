package com.crosssoftware.erecepta.model.dao;

import java.util.List;

import javax.ejb.Local;

import com.crosssoftware.erecepta.model.ds.Pacjent;
import com.crosssoftware.erecepta.model.ds.Realizacjarecepty;
import com.crosssoftware.erecepta.model.ds.Recepta;

@Local
public interface RealizacjaReceptyDao {
	Realizacjarecepty getRealizacjaRecepty(Recepta recepta);
	void update(Realizacjarecepty recepta);
	void zapisz(Realizacjarecepty realizacja);
}
