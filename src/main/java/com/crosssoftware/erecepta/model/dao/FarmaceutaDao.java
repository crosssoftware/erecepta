package com.crosssoftware.erecepta.model.dao;

import com.crosssoftware.erecepta.model.ds.Farmaceuta;

public interface FarmaceutaDao {

	Farmaceuta getByIdUzytkownika(long id);
}
