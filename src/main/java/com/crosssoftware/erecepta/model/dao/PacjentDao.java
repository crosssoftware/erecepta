package com.crosssoftware.erecepta.model.dao;

import java.util.List;

import com.crosssoftware.erecepta.model.ds.Pacjent;

public interface PacjentDao {

	void save(Pacjent pacjent);

	void update(Pacjent thisPacjent);

	List<Pacjent> getAllPacjenci(String pesel, String imie, String nazwisko);
	
	List<Pacjent> getAllPacjenci(String pesel);

	boolean doesExist(Pacjent thisPacjent);
	
	Pacjent getByPesel(String pesel);
	
	Pacjent getByIdUzytkownika(int idUzytkownika);

	boolean doesExist(String pesel);
	List<Pacjent> pobierzWszystkich();
	
	boolean doesExistWithoutThis(Pacjent selectedPacjent);
}
