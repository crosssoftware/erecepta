package com.crosssoftware.erecepta.model.ds;

// Generated 2014-06-10 08:12:46 by Hibernate Tools 4.0.0

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Recepta generated by hbm2java
 */
@Entity
@Table(name = "recepta", schema = "public")
@SequenceGenerator(name = "seqrecepta", sequenceName = "seqrecepta", allocationSize = 1)
public class Recepta implements java.io.Serializable {

	private long numerrecepty;
	private Pacjent pacjent;
	private Lekarz lekarz;
	private Character chorobaprzewlekla;
	private Date datawystawieniarecepty;
	private Set<Lekirecepty> lekirecepties = new HashSet<Lekirecepty>(0);
	private Set<Realizacjarecepty> realizacjarecepties = new HashSet<Realizacjarecepty>(
			0);
	private Date dataRealizacji;

	public Recepta() {
	}

	public Recepta(long numerrecepty, Pacjent pacjent, Lekarz lekarz) {
		this.numerrecepty = numerrecepty;
		this.pacjent = pacjent;
		this.lekarz = lekarz;
	}

	public Recepta(long numerrecepty, Pacjent pacjent, Lekarz lekarz,
			Character chorobaprzewlekla, Date datawystawieniarecepty,
			Set<Lekirecepty> lekirecepties,
			Set<Realizacjarecepty> realizacjarecepties) {
		this.numerrecepty = numerrecepty;
		this.pacjent = pacjent;
		this.lekarz = lekarz;
		this.chorobaprzewlekla = chorobaprzewlekla;
		this.datawystawieniarecepty = datawystawieniarecepty;
		this.lekirecepties = lekirecepties;
		this.realizacjarecepties = realizacjarecepties;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqrecepta")
	@Column(name = "numerrecepty", unique = true, nullable = false)
	public long getNumerrecepty() {
		return this.numerrecepty;
	}

	public void setNumerrecepty(long numerrecepty) {
		this.numerrecepty = numerrecepty;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idpacjenta", nullable = false)
	public Pacjent getPacjent() {
		return this.pacjent;
	}

	public void setPacjent(Pacjent pacjent) {
		this.pacjent = pacjent;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idlekarz", nullable = false)
	public Lekarz getLekarz() {
		return this.lekarz;
	}

	public void setLekarz(Lekarz lekarz) {
		this.lekarz = lekarz;
	}

	@Column(name = "chorobaprzewlekla", length = 1)
	public Character getChorobaprzewlekla() {
		return this.chorobaprzewlekla;
	}

	public void setChorobaprzewlekla(Character chorobaprzewlekla) {
		this.chorobaprzewlekla = chorobaprzewlekla;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DataRealizacji", length = 13)
	public Date getDataRealizacji() {
		return this.dataRealizacji;
	}

	public void setDataRealizacji(Date dataRealizacji) {
		this.dataRealizacji = dataRealizacji;
	}
	
	

	@Temporal(TemporalType.DATE)
	@Column(name = "datawystawieniarecepty", length = 13)
	public Date getDatawystawieniarecepty() {
		return this.datawystawieniarecepty;
	}

	public void setDatawystawieniarecepty(Date datawystawieniarecepty) {
		this.datawystawieniarecepty = datawystawieniarecepty;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "recepta")
	public Set<Lekirecepty> getLekirecepties() {
		return this.lekirecepties;
	}

	public void setLekirecepties(Set<Lekirecepty> lekirecepties) {
		this.lekirecepties = lekirecepties;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "recepta")
	public Set<Realizacjarecepty> getRealizacjarecepties() {
		return this.realizacjarecepties;
	}

	public void setRealizacjarecepties(
			Set<Realizacjarecepty> realizacjarecepties) {
		this.realizacjarecepties = realizacjarecepties;
	}

}
