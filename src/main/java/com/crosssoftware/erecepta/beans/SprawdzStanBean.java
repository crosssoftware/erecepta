package com.crosssoftware.erecepta.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;

import com.crosssoftware.erecepta.bo.LekireceptyBo;
import com.crosssoftware.erecepta.bo.PacjentBo;
import com.crosssoftware.erecepta.bo.RealizacjaReceptyBo;
import com.crosssoftware.erecepta.bo.ReceptyBo;
import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.Pacjent;
import com.crosssoftware.erecepta.model.ds.Realizacjarecepty;
import com.crosssoftware.erecepta.model.ds.Recepta;

@ManagedBean
@SessionScoped
public class SprawdzStanBean {

	@EJB
	private PacjentBo pacjentBo;
	@EJB
	private ReceptyBo receptyBo;
	@EJB
	private LekireceptyBo lekiReceptyBo;
	@EJB
	private RealizacjaReceptyBo realizacjaReceptyBo;
	
	private List<Pacjent> lista = new ArrayList<Pacjent>();
	private Pacjent selectedPac = new Pacjent();
	private String peselText;
	private List<Recepta> recepty;
	private List<Lekirecepty> lekiRecepty;
	private Realizacjarecepty realizacjaRecepty;
	private Recepta selectedRecepta;
	


	public Realizacjarecepty getRealizacjaRecepty() {
		return realizacjaRecepty;
	}


	public void setRealizacjaRecepty(Realizacjarecepty realizacjaRecepty) {
		this.realizacjaRecepty = realizacjaRecepty;
	}


	public List<Lekirecepty> getLekiRecepty() {
		return lekiRecepty;
	}


	public void setLekiRecepty(List<Lekirecepty> leki) {
		this.lekiRecepty = leki;
	}


	public PacjentBo getPacjentBo() {
		return pacjentBo;
	}


	public void setPacjentBo(PacjentBo pacjentBo) {
		this.pacjentBo = pacjentBo;
	}




	public List<Pacjent> getLista() {
		return lista;
	}




	public void setLista(List<Pacjent> lista) {
		this.lista = lista;
	}




	public Pacjent getSelectedPac() {
		return selectedPac;
	}




	public void setSelectedPac(Pacjent selectedPac) {
		this.selectedPac = selectedPac;
	}




	public String getPeselText() {
		return peselText;
	}




	public void setPeselText(String peselText) {
		this.peselText = peselText;
	}




	public List<Recepta> getRecepty() {
		return recepty;
	}




	public void setRecepty(List<Recepta> recepty) {
		this.recepty = recepty;
	}




	public ReceptyBo getReceptyBo() {
		return receptyBo;
	}




	public void setReceptyBo(ReceptyBo receptyBo) {
		this.receptyBo = receptyBo;
	}




	public Recepta getSelectedRecepta() {
		return selectedRecepta;
	}


	public void setSelectedRecepta(Recepta selectedRec) {
		this.selectedRecepta = selectedRec;
	}


	public String getAllPacjent() {
		this.lista.clear();
		this.lista.addAll(pacjentBo.pobierzPacjentow(this.peselText));
		if(this.lista.size()>0){
			this.selectedPac = this.lista.get(0);
			this.recepty = new ArrayList<Recepta>();
			this.recepty.clear();
			this.recepty = this.receptyBo.pobierzRecepty(this.selectedPac);
			if(this.recepty.size()>0){
				for(Recepta r : this.recepty){
					this.realizacjaRecepty = this.realizacjaReceptyBo.pobierzRealizacjeRecepty(r);
					}
				}
			return "sprawdzRecepteWybor";
		}else{
			RequestContext.getCurrentInstance().execute(
					"myForm:dialogError.show()");
			return null;
		}
	}
	public String szczegolyRecepty(){
		if(this.selectedRecepta != null){
			this.lekiRecepty = new ArrayList<Lekirecepty>();
			this.lekiRecepty.clear();
			this.lekiRecepty.addAll(this.lekiReceptyBo.pobierzLekiRecepty(this.selectedRecepta));
			System.out.println(this.lekiRecepty.size());
			System.out.println("-----------------------------");

//			this.realizacjaRecepty = new ArrayList<Realizacjarecepty>();
//			this.realizacjaRecepty.clear();
//			this.realizacjaRecepty.addAll(this.realizacjaReceptyBo.pobierzRealizacjeRecepty(this.selectedRecepta));
			return "sprawdzReceptySzczegoly";
		}else{
			System.out.println("????????????????????????????????");
			return null;
		}
	}
	public boolean czyPrzewlekla() {
		return selectedRecepta.getChorobaprzewlekla() == 't' ? true : false;
	}
	
	public long getLiczbaLekow(Recepta recepta) {
		return lekiReceptyBo.countLiczbaLekow(recepta);
	}
}
