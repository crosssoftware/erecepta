package com.crosssoftware.erecepta.beans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

import com.crosssoftware.erecepta.bo.PacjentBo;
import com.crosssoftware.erecepta.model.ds.Pacjent;

@ManagedBean
@ViewScoped
public class PacjentBean {

	@EJB
	private PacjentBo pacjentBo;
	private Pacjent thisPacjent = new Pacjent();

	public void addPacjent() {
		if (!pacjentBo.czyIstnieje(thisPacjent)) {
			pacjentBo.zapiszPacjenta(thisPacjent);
			RequestContext.getCurrentInstance().execute(
					"myForm:dialogSukces.show()");
		} else {
			RequestContext.getCurrentInstance().execute(
					"myForm:dialogError.show()");
		}
	}

	public void setPesel(String pesel) {
		thisPacjent.setPesel(pesel);
	}

	public String getPesel() {
		return thisPacjent.getPesel();
	}

	public void setImie(String imie) {
		thisPacjent.setImie(imie);
	}

	public String getImie() {
		return thisPacjent.getImie();
	}

	public void setNazwisko(String nazwisko) {
		thisPacjent.setNazwisko(nazwisko);
	}

	public String getNazwisko() {
		return thisPacjent.getNazwisko();
	}

	public void setMiejsowosc(String miejsowosc) {
		thisPacjent.setMiejsowosc(miejsowosc);
	}

	public String getMiejsowosc() {
		return thisPacjent.getMiejsowosc();
	}

	public void setUlica(String ulica) {
		thisPacjent.setUlica(ulica);
	}

	public String getUlica() {
		return thisPacjent.getUlica();
	}

	public void setNumer(String numer) {
		thisPacjent.setNumer(numer);
	}

	public String getNumer() {
		return thisPacjent.getNumer();
	}

	public void setKodpocztowy(String kodPocztowy) {
		thisPacjent.setKodpocztowy(kodPocztowy);
	}

	public String getKodpocztowy() {
		return thisPacjent.getKodpocztowy();
	}


	public void setKodnfz(String kodnfz) {
		thisPacjent.setKodnfz(kodnfz);
	}

	public String getKodnfz() {
		return thisPacjent.getKodnfz();
	}

	public void setKoduprawnien(String koduprawnien) {
		thisPacjent.setKoduprawnien(koduprawnien);
	}

	public String getKoduprawnien() {
		return thisPacjent.getKoduprawnien();
	}

}