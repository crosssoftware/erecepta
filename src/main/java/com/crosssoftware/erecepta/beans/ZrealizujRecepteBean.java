package com.crosssoftware.erecepta.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.hamcrest.core.IsEqual;
import org.hibernate.validator.xml.GetterType;
import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;

import com.crosssoftware.erecepta.bo.FarmaceutaBo;
import com.crosssoftware.erecepta.bo.LekiBoImpl;
import com.crosssoftware.erecepta.bo.LekireceptyBo;
import com.crosssoftware.erecepta.bo.PacjentBo;
import com.crosssoftware.erecepta.bo.RealizacjaReceptyBo;
import com.crosssoftware.erecepta.bo.ReceptyBo;
import com.crosssoftware.erecepta.model.ds.Farmaceuta;
import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.Pacjent;
import com.crosssoftware.erecepta.model.ds.Realizacjarecepty;
import com.crosssoftware.erecepta.model.ds.RealizacjareceptyId;
import com.crosssoftware.erecepta.model.ds.Recepta;

@ManagedBean
@SessionScoped
public class ZrealizujRecepteBean {

	private static final Logger LOG = Logger.getLogger(ZrealizujRecepteBean.class);
	
	@EJB
	private PacjentBo pacjentBo;
	@EJB
	private ReceptyBo receptyBo;
	@EJB
	private LekireceptyBo lekiReceptyBo;
	@EJB
	private RealizacjaReceptyBo realizacjaReceptyBo;
	@EJB
	private FarmaceutaBo farmaceutaBo;

	@ManagedProperty(value = "#{authorizationBean}")
	private AuthorizationBean authorizationBean;

	private List<Pacjent> lista = new ArrayList<Pacjent>();
	private Pacjent selectedPac = new Pacjent();
	private String peselText;
	private List<Recepta> recepty;
	private List<Lekirecepty> selectedLeki;
	private List<Lekirecepty> lekiRecepty;
	private Recepta selectedRecepta;
	private List<Realizacjarecepty> selectedRealizacja;
	private Date datarealizacji = new Date();

	private List<Recepta> receptaNiezrealizowane;

	private Integer intervalTime = 999999;

	private Farmaceuta farmaceuta;

	private String isRealizacja = new String();
	
	public AuthorizationBean getAuthorizationBean() {
		return authorizationBean;
	}

	public void setAuthorizationBean(AuthorizationBean authorizationBean) {
		this.authorizationBean = authorizationBean;
	}

	public Integer getIntervalTime() {
		return intervalTime;
	}

	public void setIntervalTime(Integer intervalTime) {
		this.intervalTime = intervalTime;
	}

	public List<Recepta> getReceptaNiezrealizowane() {
		return receptaNiezrealizowane;
	}

	public void setReceptaNiezrealizowane(List<Recepta> receptaNiezrealizowane) {
		this.receptaNiezrealizowane = receptaNiezrealizowane;
	}

	public List<Lekirecepty> getLekiRecepty() {
		return lekiRecepty;
	}

	public void setLekiRecepty(List<Lekirecepty> leki) {
		this.lekiRecepty = leki;
	}

	public List<Lekirecepty> getSelectedLeki() {
		return selectedLeki;
	}

	public void setSelectedLeki(List<Lekirecepty> selectedLeki) {
		this.selectedLeki = selectedLeki;
	}

	public PacjentBo getPacjentBo() {
		return pacjentBo;
	}

	public void setPacjentBo(PacjentBo pacjentBo) {
		this.pacjentBo = pacjentBo;
	}

	public List<Pacjent> getLista() {
		return lista;
	}

	public void setLista(List<Pacjent> lista) {
		this.lista = lista;
	}

	public Pacjent getSelectedPac() {
		return selectedPac;
	}

	public void setSelectedPac(Pacjent selectedPac) {
		this.selectedPac = selectedPac;
	}

	public String getPeselText() {
		return peselText;
	}

	public void setPeselText(String peselText) {
		this.peselText = peselText;
	}

	public List<Recepta> getRecepty() {
		return recepty;
	}

	public void setRecepty(List<Recepta> recepty) {
		this.recepty = recepty;
	}

	public ReceptyBo getReceptyBo() {
		return receptyBo;
	}

	public void setReceptyBo(ReceptyBo receptyBo) {
		this.receptyBo = receptyBo;
	}

	public Recepta getSelectedRecepta() {
		return selectedRecepta;
	}

	public void setSelectedRecepta(Recepta selectedRec) {
		this.selectedRecepta = selectedRec;
	}

	public String getAllPacjent() {
		this.lista.clear();
		this.lista.addAll(pacjentBo.pobierzPacjentow(this.peselText));
		if (this.lista.size() > 0) {
			this.selectedPac = this.lista.get(0);
			//
				this.recepty = new ArrayList<Recepta>();
				this.receptaNiezrealizowane = new ArrayList<Recepta>();
				this.recepty.clear();
				this.recepty.addAll(this.receptyBo.pobierzRecepty(this.selectedPac));
					if (this.recepty.size() > 0) {
						for (Recepta r : this.recepty) {
							LOG.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+czyZrealizowana(r));
							LOG.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+receptaNiezrealizowane.size());
							if (czyZrealizowana(r) == "niezrealizowana") {
								this.receptaNiezrealizowane.add(r);
								if (this.receptaNiezrealizowane.size() > 0){
									isRealizacja = "zrealizujRecepteLista";
								}//else //if (this.receptaNiezrealizowane.size() <= 0){
									//	RequestContext.getCurrentInstance().execute("myForm:dialogBrakRecept.show()");
										//isRealizacja = null;
								 	  }
							if (this.receptaNiezrealizowane.size() <= 0){
								RequestContext.getCurrentInstance().execute("myForm:dialogBrakRecept.show()");
								isRealizacja = null;
						 	  }
							//else {isRealizacja = null;RequestContext.getCurrentInstance().execute("myForm:dialogBrakRecept.show()");}
						}
					} else {isRealizacja = null;RequestContext.getCurrentInstance().execute("myForm:dialogBrakRecept.show()");}
			//
			return isRealizacja;
		} else {RequestContext.getCurrentInstance().execute("myForm:dialogError.show()");
			return null;
		}
	}

	public String czyZrealizowana(Recepta recepta) {
		List<Lekirecepty> leki = lekiReceptyBo.getAllByRecepta(recepta);
		for (Lekirecepty lek : leki) {
			if (lek.getStatus() == false) {
				return "niezrealizowana";
			}
		}
		return "zrealizowana";
	}

	public void getAllRecepty() {
		isRealizacja = null;
		// this.intervalTime = 1;
		System.out.println("//////////" + intervalTime);
		this.recepty = new ArrayList<Recepta>();
		this.receptaNiezrealizowane = new ArrayList<Recepta>();
		this.recepty.clear();
		this.recepty.addAll(this.receptyBo.pobierzRecepty(this.selectedPac));
		if (this.recepty.size() > 0) {
			LOG.info("długosc recept"+recepty.size());
			for (Recepta r : this.recepty) {
				LOG.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+czyZrealizowana(r));
				LOG.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+receptaNiezrealizowane.size());
				if (czyZrealizowana(r) == "niezrealizowana") {
					LOG.info("=========================");
					this.receptaNiezrealizowane.add(r);
					LOG.info(receptaNiezrealizowane.size());
					if (this.receptaNiezrealizowane.size() > 0) {
						isRealizacja = "zrealizujRecepteLista";
					}
					isRealizacja = null;// RequestContext.getCurrentInstance().execute("myForm:dialogBrakRecept.show()");
				}
				if (this.receptaNiezrealizowane.size() <= 0) {
					LOG.info("------------------------");
					isRealizacja = null;
				// RequestContext.getCurrentInstance().execute("myForm:dialogBrakRecept.show()");
				}
			}
				
				//else RequestContext.getCurrentInstance().execute("myForm:dialogBrakRecept.show()");
			}
		isRealizacja = null;
	 //else RequestContext.getCurrentInstance().execute("myForm:dialogBrakRecept.show()");
	}

	public String szczegolyRecepty() {
		if (this.selectedRecepta != null) {
			this.lekiRecepty = new ArrayList<Lekirecepty>();
			this.lekiRecepty.clear();
			this.lekiRecepty.addAll(this.lekiReceptyBo
					.pobierzLekiRecepty(this.selectedRecepta));
			System.out.println(this.lekiRecepty.size());
			return "zrealizujRecepteSzczegoly";
		} else {
			return null;
		}
	}

	public void realizujRecepte() {
		if (this.selectedLeki.size() > 0) {
			for (Lekirecepty lr : this.selectedLeki) {
				if (lr.getStatus() == false) {
					lr.setStatus(true);
					lekiReceptyBo.edytujLek(lr);
					RequestContext.getCurrentInstance().execute(
							"myForm:aktualizacjaLekow.show()");
				} else if (lr.getStatus() == true) {
					this.recepty.remove(lr);
					RequestContext.getCurrentInstance().execute(
							"myForm:bladWybrania.show()");
				}
			}
			
			boolean isRealized = true;
			long numerRecepty = 0;
			for (Lekirecepty iterable_element : selectedLeki) {
				numerRecepty = iterable_element.getId().getNumerrecepty();
				if(iterable_element.getStatus() == false) isRealized = false;
			}
			
			if(isRealized) {
				receptyBo.zrealizuj(numerRecepty, new Date());
			}
			
			for (Lekirecepty l : this.selectedLeki) {
				if (l.getStatus() == true) {
					Realizacjarecepty realizacja = realizacjaReceptyBo
							.pobierzRealizacjeRecepty(selectedRecepta);
					if (realizacja == null) {
						realizacja = new Realizacjarecepty();
					}
					RealizacjareceptyId id = new RealizacjareceptyId();
					id.setIdfarmaceuty(farmaceuta.getIdfarmaceuty());
					id.setNumerrecepty(selectedRecepta.getNumerrecepty());
					realizacja.setId(id);
					farmaceuta = farmaceutaBo
							.getByIdUzytkownika(authorizationBean.getThisUser()
									.getIduzytkownika());
					realizacja.setDatazrealizowaniarecepty(datarealizacji);
					realizacja.setRecepta(selectedRecepta);
					realizacja.setFarmaceuta(farmaceuta);
					realizacjaReceptyBo.zapisz(realizacja);
				}
			}

		} else {
			RequestContext.getCurrentInstance().execute(
					"myForm:bladWybrania.show()");
		}

	}

	public long getLiczbaLekow(Recepta recepta) {
		return receptyBo.getLiczbaLekow(recepta);
	}

	public boolean czyPrzewlekla() {
		return selectedRecepta.getChorobaprzewlekla() == 't' ? true : false;
	}

	public String wrocDoListy() {
		getAllRecepty();
		return "zrealizujRecepteLista";
	}

	public String wrocDoGlownej() {
		return "hello";
	}
	
}