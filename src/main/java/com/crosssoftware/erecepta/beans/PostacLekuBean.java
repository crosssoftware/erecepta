package com.crosssoftware.erecepta.beans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import com.crosssoftware.erecepta.bo.LekiBo;
import com.crosssoftware.erecepta.bo.PostacLekuBo;
import com.crosssoftware.erecepta.model.ds.Postacleku;

@ManagedBean
public class PostacLekuBean {

	@EJB
	private PostacLekuBo postacLekuBo;
	
	@EJB
	private LekiBo lekiBo;
	
	private Postacleku thisPostacLeku = new Postacleku();

	public List<Postacleku> getAllPostacLeku() {		
		return postacLekuBo.pobierzPostacLeku();
	}

	public String getNazwapostacileku() {
		return thisPostacLeku.getNazwapostacileku();
	}

	public void setNazwapostacileku(String nazwapostacileku) {
		thisPostacLeku.setNazwapostacileku(nazwapostacileku);
	}
}
