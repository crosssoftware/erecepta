package com.crosssoftware.erecepta.beans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.bo.FarmaceutaBo;
import com.crosssoftware.erecepta.bo.LekarzBo;
import com.crosssoftware.erecepta.bo.PacjentBo;
import com.crosssoftware.erecepta.bo.UserBo;
import com.crosssoftware.erecepta.model.ds.Uzytkownik;

@ManagedBean
@SessionScoped 
public class AuthorizationBean {
	
	private static final Logger LOG = Logger.getLogger(AuthorizationBean.class);
	
	@EJB
	private UserBo userBo;
	
	@EJB
	private LekarzBo lekarzBo;
	
	@EJB 
	private PacjentBo pacjentBo;
	
	@EJB
	private FarmaceutaBo farmaceutaBo;
	
	
	
	private boolean isLekarz;
	private boolean isPacjent;
	private boolean isFarmaceuta;
	private boolean isMZ;
	
	private Uzytkownik currentUser = new Uzytkownik();
	private boolean isLogedIn;
	
	public Uzytkownik getThisUser() {
		return currentUser;
	}

	public String getUserName() {
		return currentUser.getLogin();
	}

	public void setUserName(String userName) {
		currentUser.setLogin(userName);
	}

	public boolean isLogedIn() {
		return isLogedIn;
	}

	public void setLogedIn(boolean isLogedIn) {
		this.isLogedIn = isLogedIn;
	}
	
	public String getPassword() {
		return currentUser.getHaslo();
	}
	
	public void setPassword(String password) {
		this.currentUser.setHaslo(password);
	}

	public String login() {
		if(userBo.isValidLogin(currentUser)) {
			isLogedIn = true;
			currentUser = userBo.getByNazwa(currentUser);
			System.out.println("-------------------");
			isLekarz = lekarzBo.getById(currentUser.getIduzytkownika()) != null;
			isPacjent = pacjentBo.getByIdUzytkownika(currentUser.getIduzytkownika()) != null;
			isFarmaceuta = farmaceutaBo.getByIdUzytkownika(currentUser.getIduzytkownika()) != null;
			isMZ = false;

			return "loginSuccess";
		} else {
			return "loginFailure";
		}
	}

	public String logout() {
		isLogedIn = false;
		currentUser = new Uzytkownik();
		System.out.println("????????????");
		return "login";
	}
	
	public boolean isMZ() {
		if (isLogedIn) {
			return isMZ;
		} else {
			return false;
		}
	}
	
	public boolean isLekarz() {
		if (isLogedIn) {
			return isLekarz;
		} else {
			return false;
		}
	}
	
	public boolean isPacjent() {
		if (isLogedIn) {
			return isPacjent;
		} else {
			return false;
		}
	}
	
	public boolean isFarmaceuta() {
		if (isLogedIn) {
			return isFarmaceuta;
		} else {
			return false;
		}
	}
	
	public String getSignValue() {
		if (isLogedIn) {
			return "../public/logout.xhtml";
		} else {
			return "../public/login.xhtml";
		}
	}
	
	public String getSignText() {
		if (isLogedIn) {
			return "Wyloguj się";
		} else {
			return "Zaloguj się";
		}
	}
}
