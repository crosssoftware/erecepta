package com.crosssoftware.erecepta.beans;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;

import com.crosssoftware.erecepta.bo.LekiBo;
import com.crosssoftware.erecepta.model.ds.Lek;

@ManagedBean
@SessionScoped
public class EdytujLekBean {

	@EJB
	private LekiBo lekiBo;

	private List<Lek> lista = new ArrayList<Lek>();
	private Lek selectedLek;
	private String nazwa;

	public Lek getSelectedLek() {
		return selectedLek;
	}

	public void setSelectedLek(Lek selectedLek) {
		this.selectedLek = selectedLek;
	}

	public List<Lek> getLista() {
		return lista;
	}

	public void setLista(List<Lek> lista) {
		this.lista = lista;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getAllLeks() {
		
		this.lista.clear();
		this.lista.addAll(lekiBo.pobierzLeki(this.nazwa));
		if(this.lista.size()>0){
			this.selectedLek = null;	
			return "edytujLekLista";
		}else{
			RequestContext.getCurrentInstance().execute(
					"myForm:dialogError.show()");
			return null;
		}
	}
	
	public String getPostac(int x){
		
		return lekiBo.getPostac(x);
	}
	
	public String zwrocLekDoEdycji(){
		if(this.selectedLek != null){
			return "edytujLekFormularz";
		}else{
			return null;
		}
	}
	
	public void editLek(){
		if (!lekiBo.czyIstniejeOproczTego(selectedLek)) {
			lekiBo.edytujLek(selectedLek);
			RequestContext.getCurrentInstance().execute("myForm:dialogSukces.show()");
		} else {
			RequestContext.getCurrentInstance().execute("myForm:dialogError.show()");
		}
	}
}
