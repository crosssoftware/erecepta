package com.crosssoftware.erecepta.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;

import com.crosssoftware.erecepta.bo.LekarzBo;
import com.crosssoftware.erecepta.bo.LekiBo;
import com.crosssoftware.erecepta.bo.PostacLekuBo;
import com.crosssoftware.erecepta.bo.ReceptyBo;
import com.crosssoftware.erecepta.model.dao.LekDaoImpl;
import com.crosssoftware.erecepta.model.ds.Lek;
import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.LekireceptyId;
import com.crosssoftware.erecepta.model.ds.Pacjent;
import com.crosssoftware.erecepta.model.ds.Postacleku;
import com.crosssoftware.erecepta.model.ds.Recepta;

/**
 * @author mgorgon
 */
@ManagedBean
@SessionScoped
public class ReceptaBean {
	
	private static final Logger LOG = Logger.getLogger(LekDaoImpl.class);
	
	private Recepta recepta = new Recepta();
	private List<Lekirecepty> leki = new ArrayList<>();
	private List<Postacleku> postacieLeku = new ArrayList<>();
	private List<String> wielkosciOpakowania = new ArrayList<>();
	private String pesel;
	private String nazwaLeku;
	private int idPostaciLeku;
	private String wielkoscOpakowania;
	private int liczbaOpakowan = 1;
	private String dawkowanie;
	private Lekirecepty aktywnyLek;
	private boolean chorobaprzewlekla;
	private boolean maxLekow = false;
	
	@ManagedProperty(value="#{authorizationBean}")
    private AuthorizationBean authorizationBean;
	
	@EJB
	private ReceptyBo receptyBo;
	
	@EJB
	private LekiBo lekiBo;
	
	@EJB
	private PostacLekuBo postacLekuBo;
	
	@EJB
	private LekarzBo lekarzBo;
	
	public void addRecepta() {
		if (!receptyBo.czyPacjentIstnieje(pesel)) {
			RequestContext.getCurrentInstance().execute("wystawRecepteForm:dialogWrongPesel.show()");
		} else {
			Pacjent pacjent = receptyBo.getPacjentByPesel(pesel);
			recepta.setPacjent(pacjent);
			recepta.setLekarz(lekarzBo.getById(authorizationBean.getThisUser().getIduzytkownika()));
			recepta.setChorobaprzewlekla(chorobaprzewlekla ? 't' : 'n');
			recepta.setDatawystawieniarecepty(new Date());
			receptyBo.zapiszRecepte(recepta, leki);
			LOG.info("Recepta została dodana");
			RequestContext.getCurrentInstance().execute("wystawRecepteForm:dialogReceptaWystawiona.show()");
			FacesContext
			 .getCurrentInstance()
			 .getApplication()
			 .createValueBinding( "#{receptaBean}").setValue(FacesContext.getCurrentInstance(), null );
		}
	}
	
	public String refresh(){

	    return "wystaw_recepte?faces-redirect=true";
	}
	
	public void dodajLek() {
		if (liczbaOpakowan > 0) {
			Lek nowyLek = lekiBo.pobierzLek(nazwaLeku, idPostaciLeku, wielkoscOpakowania);
			boolean jest = false;
			for (Lekirecepty lek : leki) {
				if (nowyLek.getIdleku() == lek.getLek().getIdleku()) {
					jest = true;
				}
			}
			if (!jest) {
				LOG.info("Dodanie leku o nazwie " + nazwaLeku + " do recepty.");
				LekireceptyId lekireceptyId = new LekireceptyId(nowyLek.getIdleku(), receptyBo.count());
				Lekirecepty daneLeku = new Lekirecepty(lekireceptyId, recepta, nowyLek, Integer.valueOf(liczbaOpakowan), dawkowanie, false);
				leki.add(daneLeku);
			}
			maxLekow = leki.size() >= 5;
		} else {
			RequestContext.getCurrentInstance().execute("wystawRecepteForm:dialogWrongNumber.show()");
		}
	}
	
	public void usunLek() {
		LOG.info("Usunięcie leku z recepty.");
		leki.remove(aktywnyLek);
		maxLekow = leki.size() >= 5;
	}
	
	public void getAllPostacLekuForNazwaLeku() {
		LOG.info("Pobieranie postaci leku dla " + nazwaLeku);
		ArrayList<Postacleku> postacieLekuDlaLeku = new ArrayList<>();
		List<Lek> leki = lekiBo.pobierzLeki(nazwaLeku);
		for (Lek lek : leki) {
			int idPostaciLeku = lek.getIdpostacileku();
			Postacleku postacLeku = postacLekuBo.getPostacLekuById(idPostaciLeku);
			boolean jest = false;
			for (Postacleku postac : postacieLekuDlaLeku) {
				if (postacLeku.getIdpostacileku() == postac.getIdpostacileku()) {
					jest = true;
				}
			}
			if (!jest) {
				postacieLekuDlaLeku.add(postacLeku);
			}
		}
		idPostaciLeku = 0;
		liczbaOpakowan = 1;
		dawkowanie = "";
		postacieLeku = postacieLekuDlaLeku;
	}
	
	public void getAllWielkosciOpakowaniaForPostacLeku() {
		LOG.info("Pobieranie wielkości opakowania leku");
		List<String> wielkosci = new ArrayList<>();
		List<Lek> leki = lekiBo.pobierzLeki(nazwaLeku, idPostaciLeku);
		for (Lek lek : leki) {
			String wielkoscOpakowania = lek.getWielkoscopakowania();
			wielkosci.add(wielkoscOpakowania);
		}
		wielkosciOpakowania = wielkosci;
	}
	
	public String getNazwaPostaciLeku(Lek lek) {
		Postacleku postacLeku = postacLekuBo.getPostacLekuById(lek.getIdpostacileku());
		return postacLeku.getNazwapostacileku();
	}
	
	public List<Postacleku> getPostacieLeku() {
		return postacieLeku;
	}

	public void setPostacieLeku(List<Postacleku> postacieLeku) {
		this.postacieLeku = postacieLeku;
	}

	public List<Lekirecepty> getLeki() {
		return leki;
	}

	public void setLeki(List<Lekirecepty> leki) {
		this.leki = leki;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getNazwaLeku() {
		return nazwaLeku;
	}

	public void setNazwaLeku(String nazwaLeku) {
		this.nazwaLeku = nazwaLeku;
	}

	public int getIdPostaciLeku() {
		return idPostaciLeku;
	}

	public void setIdPostaciLeku(int idPostaciLeku) {
		this.idPostaciLeku = idPostaciLeku;
	}

	public int getLiczbaOpakowan() {
		return liczbaOpakowan;
	}

	public void setLiczbaOpakowan(int liczbaOpakowan) {
		this.liczbaOpakowan = liczbaOpakowan;
	}

	public String getDawkowanie() {
		return dawkowanie;
	}

	public void setDawkowanie(String dawkowanie) {
		this.dawkowanie = dawkowanie;
	}

	public Lekirecepty getAktywnyLek() {
		return aktywnyLek;
	}

	public void setAktywnyLek(Lekirecepty aktywnyLek) {
		this.aktywnyLek = aktywnyLek;
	}

	public boolean isChorobaprzewlekla() {
		return chorobaprzewlekla;
	}

	public void setChorobaprzewlekla(boolean chorobaprzewlekla) {
		this.chorobaprzewlekla = chorobaprzewlekla;
	}

	public AuthorizationBean getAuthorizationBean() {
		return authorizationBean;
	}

	public void setAuthorizationBean(AuthorizationBean authorizationBean) {
		this.authorizationBean = authorizationBean;
	}

	public boolean isMaxLekow() {
		return maxLekow;
	}

	public void setMaxLekow(boolean maxLekow) {
		this.maxLekow = maxLekow;
	}

	public String getWielkoscOpakowania() {
		return wielkoscOpakowania;
	}

	public void setWielkoscOpakowania(String wielkoscOpakowania) {
		this.wielkoscOpakowania = wielkoscOpakowania;
	}

	public List<String> getWielkosciOpakowania() {
		return wielkosciOpakowania;
	}

	public void setWielkosciOpakowania(List<String> wielkosciOpakowania) {
		this.wielkosciOpakowania = wielkosciOpakowania;
	}
}