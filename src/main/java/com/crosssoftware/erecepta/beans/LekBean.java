package com.crosssoftware.erecepta.beans;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

import com.crosssoftware.erecepta.bo.LekiBo;
import com.crosssoftware.erecepta.model.ds.Lek;

/**
 * @author mgorgon
 */
@ManagedBean
@ViewScoped
public class LekBean {
	
	@EJB
	private LekiBo lekiBo;
	private Lek thisLek = new Lek();

	public void addLek() {
		if (!lekiBo.czyIstnieje(thisLek)) {
			lekiBo.zapiszLek(thisLek);
			RequestContext.getCurrentInstance().execute("myForm:dialogSukces.show()");
		} else {
			RequestContext.getCurrentInstance().execute("myForm:dialogError.show()");
		}
	}

	public BigDecimal getCenadetaliczna() {
		return thisLek.getCenadetaliczna();
	}

	public void setCenadetaliczna(BigDecimal cenadetaliczna) {
		thisLek.setCenadetaliczna(cenadetaliczna);
	}

	public BigDecimal getCenaprzyrefundacji() {
		return thisLek.getCenaprzyrefundacji();
	}

	public void setCenaprzyrefundacji(BigDecimal cenaprzyrefundacji) {
		thisLek.setCenaprzyrefundacji(cenaprzyrefundacji);
	}

	public String getWielkoscopakowania() {
		return thisLek.getWielkoscopakowania();
	}

	public void setWielkoscopakowania(String wielkoscopakowania) {
		thisLek.setWielkoscopakowania(wielkoscopakowania);
	}

	public int getIdpostacileku() {
		return thisLek.getIdpostacileku();
	}

	public void setIdpostacileku(int idpostacileku) {
		thisLek.setIdpostacileku(idpostacileku);
	}

	public String getNazwaleku() {
		return thisLek.getNazwaleku();
	}

	public void setNazwaleku(String nazwaleku) {
		thisLek.setNazwaleku(nazwaleku);
	}

	public String getSklad() {
		return thisLek.getSklad();
	}

	public void setSklad(String sklad) {
		thisLek.setSklad(sklad);
	}

	public String getDawkasubstancjiczynnej() {
		return thisLek.getDawkasubstancjiczynnej();
	}

	public void setDawkasubstancjiczynnej(String dawkasubstancjiczynnej) {
		thisLek.setDawkasubstancjiczynnej(dawkasubstancjiczynnej);
	}
	
	public List<String> pobierzLekiRozpoczynajaceSieOd(String query) {
		List<String> leki = lekiBo.pobierzLekiRozpoczynajaceSieOd(query);
		// usunięcie duplikatów
		HashSet hs = new HashSet();
		hs.addAll(leki);
		leki.clear();
		leki.addAll(hs);
		return leki;
	}
}
