package com.crosssoftware.erecepta.beans;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;

import com.crosssoftware.erecepta.bo.PacjentBo;
import com.crosssoftware.erecepta.model.ds.Pacjent;

@ManagedBean
@SessionScoped
public class EdytujPacBean {

	@EJB
	private PacjentBo pacjentBo;

	private List<Pacjent> lista = new ArrayList<Pacjent>();
	private Pacjent selectedPac;
	private String pesel;
	private String imie;
	private String nazwisko;

	public List<Pacjent> getLista() {
		return lista;
	}

	public void setLista(List<Pacjent> lista) {
		this.lista = lista;
	}

	public Pacjent getSelectedPac() {
		return selectedPac;
	}

	public void setSelectedPac(Pacjent selectedPac) {
		this.selectedPac = selectedPac;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getAllPac() {
		this.lista.clear();
		if (this.imie == "" && this.nazwisko == "" && this.pesel == "") {
			RequestContext.getCurrentInstance().execute(
					"myForm:dialogError2.show()");
			return null;
		} else {
			this.lista.addAll(pacjentBo.pobierzPacjentow(this.pesel, this.imie,
					this.nazwisko));
			if (this.lista.size() > 0) {
				this.selectedPac = null;
				return "edytujPacjentaLista";
			} else {
				RequestContext.getCurrentInstance().execute(
						"myForm:dialogError.show()");
				return null;
			}
		}
	}

	public String zwrocPacDoEdycji() {
		if (this.selectedPac != null) {
			return "edytujPacjentaFormularz";
		} else {
			return null;
		}
	}

	public void editPacjent() {
		if (!pacjentBo.czyIstniejeOproczTego(selectedPac)) {
			pacjentBo.edytujKartePacjenta(selectedPac);
			RequestContext.getCurrentInstance().execute(
					"myForm:dialogSukces.show()");
		} else {
			RequestContext.getCurrentInstance().execute(
					"myForm:dialogError.show()");
		}
	}

}
