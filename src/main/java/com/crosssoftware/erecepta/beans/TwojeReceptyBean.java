package com.crosssoftware.erecepta.beans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;

import com.crosssoftware.erecepta.bo.LekarzBo;
import com.crosssoftware.erecepta.bo.LekiBo;
import com.crosssoftware.erecepta.bo.LekireceptyBo;
import com.crosssoftware.erecepta.bo.PacjentBo;
import com.crosssoftware.erecepta.bo.ReceptyBo;
import com.crosssoftware.erecepta.model.dao.LekDaoImpl;
import com.crosssoftware.erecepta.model.ds.Lek;
import com.crosssoftware.erecepta.model.ds.Lekarz;
import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.Pacjent;
import com.crosssoftware.erecepta.model.ds.Recepta;
import com.crosssoftware.erecepta.model.ds.Zoz;

@ManagedBean
@SessionScoped
public class TwojeReceptyBean {

	private static final Logger LOG = Logger.getLogger(LekDaoImpl.class);
	
	@EJB
	private ReceptyBo receptyBo;
	
	@EJB
	private PacjentBo pacjentBo;
	
	@EJB
	private LekarzBo lekarzBo;
	
	@EJB
	private LekireceptyBo lekireceptyBo;
	
	@EJB
	private LekiBo lekiBo;
	
	@ManagedProperty(value="#{authorizationBean}")
    private AuthorizationBean authorizationBean;
	
	@ManagedProperty(value="#{receptaBean}")
	private ReceptaBean receptaBean;
	
	public ReceptaBean getReceptaBean() {
		return receptaBean;
	}

	public void setReceptaBean(ReceptaBean receptaBean) {
		this.receptaBean = receptaBean;
	}

	private Recepta aktywnaRecepta;
	
	private Pacjent pacjent;
	
	private Lekarz lekarz;
	
	private List<Lekirecepty> leki;
	
	private Lekirecepty aktywnyLek;
	
	private String nazwaLeku;
	private String skladLeku;
	private String postacLeku;
	private String dawkaSubs;
	private String wielkosc;
	private String cenaDetal;
	private String cenaRef;
	
	public void wyswietlSzczegolyLeku() {
		nazwaLeku = getDaneLeku(aktywnyLek).getNazwaleku();
		setSkladLeku(getDaneLeku(aktywnyLek).getSklad());
		setPostacLeku(receptaBean.getNazwaPostaciLeku(getDaneLeku(aktywnyLek)));
		setDawkaSubs(getDaneLeku(aktywnyLek).getDawkasubstancjiczynnej());
		setWielkosc(getDaneLeku(aktywnyLek).getWielkoscopakowania());
		setCenaDetal(getDaneLeku(aktywnyLek).getCenadetaliczna().toString());
		setCenaRef(getDaneLeku(aktywnyLek).getCenaprzyrefundacji().toString());
		RequestContext.getCurrentInstance().execute("szczegolyReceptyForm:dialogDaneLeku.show()");
	}

	public Pacjent getPacjent() {
		if (pacjent != null) {
			return pacjent;
		} else {
			int idUzytkownika = authorizationBean.getThisUser().getIduzytkownika();
			pacjent = pacjentBo.getByIdUzytkownika(idUzytkownika);
			return pacjent;
		}
	}

	public void setPacjent(Pacjent pacjent) {
		this.pacjent = pacjent;
	}

	public List<Recepta> getReceptyPacjenta() {
		int idUzytkownika = authorizationBean.getThisUser().getIduzytkownika();
		pacjent = pacjentBo.getByIdUzytkownika(idUzytkownika);
		List<Recepta> recepty = receptyBo.pobierzRecepty(pacjent);
		if (recepty.isEmpty()) {
			RequestContext.getCurrentInstance().execute("twojeReceptyForm:dialogBrakRecept.show()");
		}
		return recepty;
	}
	
	public String getZozName(Lekarz lekarz) {
		return lekarzBo.getZozName(lekarz);
	}
	
	public Zoz getZoz() {
		return receptyBo.getZoz(aktywnaRecepta);
	}
	
	public String getAdresZoz() {
		Zoz zoz = getZoz();
		return String.format("%s %s, %s %s", zoz.getUlica(), zoz.getNumer(), zoz.getKodpocztowy(), zoz.getMiejsowosc());
	}
	
	public String getAdresPacjent() {
		return String.format("%s %s, %s %s", pacjent.getUlica(), pacjent.getNumer(), pacjent.getKodpocztowy(), pacjent.getMiejsowosc());
	}
	
	public long getLiczbaLekow(Recepta recepta) {
		return receptyBo.getLiczbaLekow(recepta);
	}
	
	public AuthorizationBean getAuthorizationBean() {
		return authorizationBean;
	}
	
	public void setAuthorizationBean(AuthorizationBean authorizationBean) {
		this.authorizationBean = authorizationBean;
	}

	public Recepta getAktywnaRecepta() {
		return aktywnaRecepta;
	}

	public void setAktywnaRecepta(Recepta aktywnaRecepta) {
		this.aktywnaRecepta = aktywnaRecepta;
	}

	public Lekarz getLekarz() {
		return aktywnaRecepta.getLekarz();
	}

	public void setLekarz(Lekarz lekarz) {
		this.lekarz = lekarz;
	}
	
	public boolean czyPrzewlekla() {
		return aktywnaRecepta.getChorobaprzewlekla() == 't' ? true : false;
	}

	public List<Lekirecepty> getLeki() {
		return lekireceptyBo.getAllByRecepta(aktywnaRecepta);
	}

	public void setLeki(List<Lekirecepty> leki) {
		this.leki = leki;
	}
	
	public Lek getDaneLeku(Lekirecepty lek) {
		return lekiBo.pobierzLek(lek);
	}

	public Lekirecepty getAktywnyLek() {
		return aktywnyLek;
	}

	public void setAktywnyLek(Lekirecepty aktywnyLek) {
		this.aktywnyLek = aktywnyLek;
	}

	public String getNazwaLeku() {
		return nazwaLeku;
	}

	public void setNazwaLeku(String nazwaLeku) {
		this.nazwaLeku = nazwaLeku;
	}

	public String getSkladLeku() {
		return skladLeku;
	}

	public void setSkladLeku(String skladLeku) {
		this.skladLeku = skladLeku;
	}

	public String getPostacLeku() {
		return postacLeku;
	}

	public void setPostacLeku(String postacLeku) {
		this.postacLeku = postacLeku;
	}

	public String getDawkaSubs() {
		return dawkaSubs;
	}

	public void setDawkaSubs(String dawkaSubs) {
		this.dawkaSubs = dawkaSubs;
	}

	public String getWielkosc() {
		return wielkosc;
	}

	public void setWielkosc(String wielkosc) {
		this.wielkosc = wielkosc;
	}

	public String getCenaDetal() {
		return cenaDetal;
	}

	public void setCenaDetal(String cenaDetal) {
		this.cenaDetal = cenaDetal;
	}

	public String getCenaRef() {
		return cenaRef;
	}

	public void setCenaRef(String cenaRef) {
		this.cenaRef = cenaRef;
	}
	
	public String czyZrealizowana(Recepta recepta) {
		List<Lekirecepty> leki = lekireceptyBo.getAllByRecepta(recepta);
		for (Lekirecepty lek : leki) {
			if (lek.getStatus() == false) {
				return "niezrealizowana";
			}
		}
		return "zrealizowana";
	}
}
