package com.crosssoftware.erecepta.bo;

import java.util.List;

import com.crosssoftware.erecepta.model.ds.Pacjent;

public interface PacjentBo {

	List<Pacjent> pobierzPacjentow(String pesel, String imie,
			String nazwisko);
	
	List<Pacjent> pobierzPacjentow(String pesel);
	
	Pacjent getByIdUzytkownika(int idUzytkownika);

	void zapiszPacjenta(Pacjent thisPacjent);

	void edytujKartePacjenta(Pacjent thisPacjent);

	boolean czyIstnieje(Pacjent thisPacjent);
	
	List<Pacjent> pobierzWszystkichPacjentow();
	
	public boolean czyIstniejeOproczTego(Pacjent selectedPacjent);
}
