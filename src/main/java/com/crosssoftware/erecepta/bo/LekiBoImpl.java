package com.crosssoftware.erecepta.bo;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.dao.LekDao;
import com.crosssoftware.erecepta.model.dao.PostacLekuDao;
import com.crosssoftware.erecepta.model.ds.Lek;
import com.crosssoftware.erecepta.model.ds.Lekirecepty;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class LekiBoImpl implements LekiBo {

	private static final Logger LOG = Logger.getLogger(LekiBoImpl.class);
	
	@EJB
	private LekDao lekDao;
	
	@EJB
	private PostacLekuDao postacLekuDao;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Lek> pobierzLeki(String nazwa) {
		LOG.info("Pobieranie leków...");
		List<Lek> leki = lekDao.getAllLeks(nazwa);
		return leki;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void zapiszLek(Lek thisLek) {
		LOG.info("Zapisywanie leku...");
		lekDao.save(thisLek);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean czyIstnieje(Lek thisLek) {
		LOG.info("Sprawdzanie czy lek istnieje...");
		return lekDao.doesExist(thisLek);
	}

	@Override
	public void edytujLek(Lek thisLek) {
		LOG.info("Edycja leku...");
		lekDao.update(thisLek);
	}

	@Override
	public boolean czyIstniejeOproczTego(Lek selectedLek) {
		LOG.info("Sprawdzanie czy lek istnieje...");
		return lekDao.doesExistWithoutThis(selectedLek);
	}

	@Override
	public List<String> pobierzLekiRozpoczynajaceSieOd(String nazwa) {
		LOG.info("Pobieranie listy leków zgodnych z zapytaniem.");
		return lekDao.pobierzLekiRozpoczynajaceSieOd(nazwa);
	}

	@Override
	public List<Lek> pobierzLeki(String nazwa, int idPostaciLeku) {
		LOG.info(String.format("Pobieranie leku o nazwie %s i idPostaciLeku %d", nazwa, idPostaciLeku));
		return lekDao.pobierzLeki(nazwa, idPostaciLeku);
	}

	@Override
	public Lek pobierzLek(Lekirecepty lek) {
		return lekDao.pobierzLek(lek);
	}

	@Override
	public Lek pobierzLek(String nazwa, int idPostaciLeku,
			String wielkoscOpakowania) {
		return lekDao.pobierzLek(nazwa, idPostaciLeku, wielkoscOpakowania);
	}

	@Override
	public String getPostac(int x) {
		LOG.info("Pobieranei postaci...");
		return lekDao.getPostacLeku(x);
	}

}
