package com.crosssoftware.erecepta.bo;

import com.crosssoftware.erecepta.model.ds.Lekarz;

public interface LekarzBo {
	
	Lekarz getById(long id);
	
	String getZozName(Lekarz lekarz);
}
