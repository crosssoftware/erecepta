package com.crosssoftware.erecepta.bo;

import com.crosssoftware.erecepta.model.ds.Farmaceuta;

public interface FarmaceutaBo {

	Farmaceuta getByIdUzytkownika(long id);
}
