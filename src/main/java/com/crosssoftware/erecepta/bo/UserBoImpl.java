package com.crosssoftware.erecepta.bo;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.crosssoftware.erecepta.model.dao.UzytkownicyDao;
import com.crosssoftware.erecepta.model.ds.Uzytkownik;

@Stateless
@LocalBean
public class UserBoImpl implements UserBo {

	@EJB
	private UzytkownicyDao userDao;

	@Override
	public boolean isValidLogin(Uzytkownik user) {
		return userDao.czyPoprawnyLogin(user);
	}

	@Override
	public Uzytkownik getByNazwa(Uzytkownik currentUser) {
		// TODO Auto-generated method stub
		return userDao.getUser(currentUser.getLogin());
	}

}
