package com.crosssoftware.erecepta.bo;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.dao.LekDao;
import com.crosssoftware.erecepta.model.dao.LekireceptyDao;
import com.crosssoftware.erecepta.model.dao.PacjentDao;
import com.crosssoftware.erecepta.model.dao.ReceptaDao;
import com.crosssoftware.erecepta.model.dao.ZozDao;
import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.Pacjent;
import com.crosssoftware.erecepta.model.ds.Recepta;
import com.crosssoftware.erecepta.model.ds.Zoz;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ReceptyBoImpl implements ReceptyBo {
	
	private static final Logger LOG = Logger.getLogger(LekiBoImpl.class);
	
	@EJB
	private ReceptaDao receptaDao;
	
	@EJB
	private LekireceptyDao lekireceptyDao;
	
	@EJB
	private PacjentDao pacjentDao;
	
	@EJB
	private LekDao lekiDao;
	
	@EJB
	private ZozDao zozDao;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void zapiszRecepte(Recepta recepta, List<Lekirecepty> leki) {
		LOG.info("Zapisywanie recepty...");
		receptaDao.save(recepta, leki);
	}

	@Override
	public Pacjent getPacjentByPesel(String pesel) {
		return pacjentDao.getByPesel(pesel);
	}

	@Override
	public long count() {
		return receptaDao.count();
	}

	@Override
	public boolean czyPacjentIstnieje(String pesel) {
		return pacjentDao.doesExist(pesel);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Recepta> pobierzRecepty(Pacjent pacjent) {
		LOG.info("Pobieranie recept...");
		List<Recepta> recepty = receptaDao.getAllRecepty(pacjent);
		return recepty;
	}

	@Override
	public long getLiczbaLekow(Recepta recepta) {
		return receptaDao.getLiczbaLekow(recepta);
	}

	@Override
	public Zoz getZoz(Recepta recepta) {
		return zozDao.getZoz(recepta);
	}

	@Override
	public void zrealizuj(long numerRecepty, Date date) {
		receptaDao.zrealizuj(numerRecepty, date);
	}
}
