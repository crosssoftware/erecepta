package com.crosssoftware.erecepta.bo;

import javax.ejb.Local;

import com.crosssoftware.erecepta.model.ds.Uzytkownik;

@Local
public interface UserBo {
	
	boolean isValidLogin(Uzytkownik thisUser);

	Uzytkownik getByNazwa(Uzytkownik currentUser);
	
}
