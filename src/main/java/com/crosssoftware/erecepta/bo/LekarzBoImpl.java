package com.crosssoftware.erecepta.bo;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.crosssoftware.erecepta.model.dao.LekarzDao;
import com.crosssoftware.erecepta.model.ds.Lekarz;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class LekarzBoImpl implements LekarzBo {

	@EJB
	LekarzDao lekarzDao;
	
	@Override
	public Lekarz getById(long id) {
		return lekarzDao.getById(id);
	}

	@Override
	public String getZozName(Lekarz lekarz) {
		return lekarzDao.getZozName(lekarz);
	}

}
