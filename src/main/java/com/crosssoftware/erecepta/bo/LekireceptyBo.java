package com.crosssoftware.erecepta.bo;

import java.util.List;

import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.Recepta;

public interface LekireceptyBo {

	List<Lekirecepty> getAllByRecepta(Recepta recepta);
	List<Lekirecepty> pobierzLekiRecepty(Recepta recepta);
	void zapiszLek(Lekirecepty thisLek);
	void edytujLek(Lekirecepty thisLek);
	long countLiczbaLekow(Recepta recepta);
}
