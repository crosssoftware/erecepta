package com.crosssoftware.erecepta.bo;

import java.util.Date;
import java.util.List;

import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.Pacjent;
import com.crosssoftware.erecepta.model.ds.Recepta;
import com.crosssoftware.erecepta.model.ds.Zoz;

/**
 * @author pawegio
 */
public interface ReceptyBo {

	void zapiszRecepte(Recepta recepta, List<Lekirecepty> leki);
	
	List<Recepta> pobierzRecepty(Pacjent pacjent);
	Pacjent getPacjentByPesel(String pesel);
	
	Zoz getZoz(Recepta recepta);
	
	boolean czyPacjentIstnieje(String pesel);
	
	long getLiczbaLekow(Recepta recepta);
	
	long count();

	void zrealizuj(long numerRecepty, Date date);
}
