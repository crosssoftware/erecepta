package com.crosssoftware.erecepta.bo;

import java.util.List;

import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.Realizacjarecepty;
import com.crosssoftware.erecepta.model.ds.Recepta;

public interface RealizacjaReceptyBo {

	Realizacjarecepty pobierzRealizacjeRecepty (Recepta recepta);
	void edytujRealizacjaRecepty(Realizacjarecepty recepta);
	void zapisz(Realizacjarecepty realizacja);
}
