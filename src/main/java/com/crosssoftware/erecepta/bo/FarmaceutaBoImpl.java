package com.crosssoftware.erecepta.bo;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.crosssoftware.erecepta.model.dao.FarmaceutaDao;
import com.crosssoftware.erecepta.model.ds.Farmaceuta;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FarmaceutaBoImpl implements FarmaceutaBo {

	@EJB
	FarmaceutaDao farmaceutaDao;
	
	@Override
	public Farmaceuta getByIdUzytkownika(long id) {
		return farmaceutaDao.getByIdUzytkownika(id);
	}

}
