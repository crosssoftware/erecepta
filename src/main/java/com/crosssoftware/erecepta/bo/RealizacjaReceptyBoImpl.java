package com.crosssoftware.erecepta.bo;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.dao.RealizacjaReceptyDao;
import com.crosssoftware.erecepta.model.ds.Realizacjarecepty;
import com.crosssoftware.erecepta.model.ds.Recepta;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class RealizacjaReceptyBoImpl implements RealizacjaReceptyBo {

private static final Logger LOG = Logger.getLogger(LekiBoImpl.class);
	
	@EJB
	private RealizacjaReceptyDao realizacjaReceptyDao;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Realizacjarecepty pobierzRealizacjeRecepty(Recepta recepta) {
		LOG.info("Pobieranie Realizacji recepty...");
		return realizacjaReceptyDao.getRealizacjaRecepty(recepta);
		
	}

	@Override
	public void edytujRealizacjaRecepty(Realizacjarecepty recepta) {
		LOG.info("Edycja leku...");
		realizacjaReceptyDao.update(recepta);
	}

	@Override
	public void zapisz(Realizacjarecepty realizacja) {
		realizacjaReceptyDao.zapisz(realizacja);
	}

}
