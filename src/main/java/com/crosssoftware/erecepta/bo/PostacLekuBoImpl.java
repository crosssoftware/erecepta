package com.crosssoftware.erecepta.bo;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.dao.PostacLekuDao;
import com.crosssoftware.erecepta.model.ds.Postacleku;

@Stateless
public class PostacLekuBoImpl implements PostacLekuBo {

	private static final Logger LOG = Logger.getLogger(PostacLekuBoImpl.class);
	
	@EJB
	private PostacLekuDao postacLekuDao;

	@Override
	public List<Postacleku> pobierzPostacLeku() {
		List<Postacleku> postacieDs = postacLekuDao.getAllPostacLeku();
		LOG.info("Odczytano: " + postacieDs.size() + " postaci leku");
		return postacieDs;
	}
	
	@Override
	public Postacleku getPostacLekuByNazwa(String nazwaPostaci) {
		return postacLekuDao.getPostacLekuByNazwa(nazwaPostaci);
	}

	@Override
	public Postacleku getPostacLekuById(int idPostaciLeku) {
		return postacLekuDao.getPostacLekuById(idPostaciLeku);
	}

}
