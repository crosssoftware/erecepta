package com.crosssoftware.erecepta.bo;

import java.util.List;

import com.crosssoftware.erecepta.model.ds.Postacleku;

public interface PostacLekuBo {
	
	List<Postacleku> pobierzPostacLeku();

	Postacleku getPostacLekuById(int idPostaciLeku);
	
	Postacleku getPostacLekuByNazwa(String nazwaPostaci);
	
}
