package com.crosssoftware.erecepta.bo;

import java.util.List;

import com.crosssoftware.erecepta.model.ds.Lek;
import com.crosssoftware.erecepta.model.ds.Lekirecepty;

/**
 * @author mgorgon
 */
public interface LekiBo {
	

	List<Lek> pobierzLeki(String nazwa);
	List<Lek> pobierzLeki(String nazwa, int idPostaciLeku);
	Lek pobierzLek(String nazwa, int idPostaciLeku, String wielkoscOpakowania);
	Lek pobierzLek(Lekirecepty lek);
	void zapiszLek(Lek thisLek);
	void edytujLek(Lek thisLek);

	boolean czyIstnieje(Lek thisLek);

	boolean czyIstniejeOproczTego(Lek selectedLek);
	
	List<String> pobierzLekiRozpoczynajaceSieOd(String nazwa);
	String getPostac(int x);
}
