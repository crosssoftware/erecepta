package com.crosssoftware.erecepta.bo;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.dao.PacjentDao;
import com.crosssoftware.erecepta.model.ds.Pacjent;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PacjentBoImpl implements PacjentBo {

	private static final Logger LOG = Logger.getLogger(PacjentBoImpl.class);

	@EJB
	private PacjentDao pacjentDao;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Pacjent> pobierzPacjentow(String pesel, String imie,
			String nazwisko) {
		LOG.info("Pobieranie pacjentow...");
		List<Pacjent> pacjenci = pacjentDao.getAllPacjenci(pesel, imie,
				nazwisko);
		return pacjenci;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Pacjent> pobierzPacjentow(String pesel) {
		LOG.info("Pobieranie pacjentow...");
		List<Pacjent> pacjenci = pacjentDao.getAllPacjenci(pesel);
		return pacjenci;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void zapiszPacjenta(Pacjent thisPacjent) {
		LOG.info("Zapisywanie leku...");
		pacjentDao.save(thisPacjent);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean czyIstnieje(Pacjent thisPacjent) {
		LOG.info("Sprawdzanie czy pacjent istnieje...");
		return pacjentDao.doesExist(thisPacjent);
	}

	@Override
	public void edytujKartePacjenta(Pacjent thisPacjent) {
		LOG.info("Edycja karty pacjenta...");
		pacjentDao.update(thisPacjent);
	}

	@Override
	public Pacjent getByIdUzytkownika(int idUzytkownika) {
		LOG.info("Pobieranie pacjenta");
		return pacjentDao.getByIdUzytkownika(idUzytkownika);
	}
		
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Pacjent> pobierzWszystkichPacjentow() {
		LOG.info("Edycja karty pacjenta...");
		List<Pacjent> pacjenci = pacjentDao.pobierzWszystkich();
		return pacjenci;
	}
	
	public boolean czyIstniejeOproczTego(Pacjent selectedPacjent) {
		LOG.info("Sprawdzanie czy pacjent istnieje oprocz tego...");
		return pacjentDao.doesExistWithoutThis(selectedPacjent);

	}

}