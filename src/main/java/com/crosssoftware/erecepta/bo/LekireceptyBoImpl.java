package com.crosssoftware.erecepta.bo;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.jboss.logging.Logger;

import com.crosssoftware.erecepta.model.dao.LekireceptyDao;
import com.crosssoftware.erecepta.model.ds.Lekirecepty;
import com.crosssoftware.erecepta.model.ds.Recepta;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class LekireceptyBoImpl implements LekireceptyBo {

	private static final Logger LOG = Logger.getLogger(LekiBoImpl.class);
	
	@EJB
	LekireceptyDao lekireceptyDao;
	
	@Override
	public List<Lekirecepty> getAllByRecepta(Recepta recepta) {
		return lekireceptyDao.getAllByRecepta(recepta);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Lekirecepty> pobierzLekiRecepty(Recepta recepta) {
		LOG.info("Pobieranie leków...");
		List<Lekirecepty> leki = lekireceptyDao.getAllLeksRecept(recepta);
		return leki;
	}

	@Override
	public void zapiszLek(Lekirecepty thisLek) {
		LOG.info("Zapisywanie leku...");
		lekireceptyDao.save(thisLek);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void edytujLek(Lekirecepty thisLek) {
		LOG.info("Edycja leku...");
		lekireceptyDao.update(thisLek);
	}

	@Override
	public long countLiczbaLekow(Recepta recepta) {
		return lekireceptyDao.countLiczbaLekow(recepta);
	}
}
