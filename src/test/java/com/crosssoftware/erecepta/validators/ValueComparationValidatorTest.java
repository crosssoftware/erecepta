package com.crosssoftware.erecepta.validators;

import java.math.BigDecimal;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.validator.ValidatorException;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author mgorgon
 */
@Test
public class ValueComparationValidatorTest {
	
	@Mock
	private UIComponent uiComponentMock;
	
	@Mock
	private Map<String, Object> attributesMock;
	
	@BeforeClass
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void validate_RzucWyjatekJesliCenaDetalicznaNizszaNizRefundacji() {
		BigDecimal cenaDetaliczna = new BigDecimal(10.0); 
		BigDecimal cenaPoRefundacji = new BigDecimal(100.0); 
		
		
		Mockito.when(uiComponentMock.getAttributes()).thenReturn(attributesMock);
		Mockito.when(attributesMock.get(ValueComparationValidator.CENA_DETALICZNA_KEY)).thenReturn(cenaDetaliczna);
		
		ValueComparationValidator validator = new ValueComparationValidator();
		
		validator.validate(null, uiComponentMock, cenaPoRefundacji);
	}
	
	public void validate_PrzepuscCeneDetalicznaJesliNizszaNizRefundacji() {
		BigDecimal cenaDetaliczna = new BigDecimal(555.0); 
		BigDecimal cenaPoRefundacji = new BigDecimal(100.0); 
		
		
		Mockito.when(uiComponentMock.getAttributes()).thenReturn(attributesMock);
		Mockito.when(attributesMock.get(ValueComparationValidator.CENA_DETALICZNA_KEY)).thenReturn(cenaDetaliczna);
		
		ValueComparationValidator validator = new ValueComparationValidator();
		
		validator.validate(null, uiComponentMock, cenaPoRefundacji);
	}
	
	public void validate_PrzepuscJesliCenaDetalicznaPusta() {
		BigDecimal cenaPoRefundacji = new BigDecimal(100.0); 
		
		
		Mockito.when(uiComponentMock.getAttributes()).thenReturn(attributesMock);
		Mockito.when(attributesMock.get(ValueComparationValidator.CENA_DETALICZNA_KEY)).thenReturn(null);
		
		ValueComparationValidator validator = new ValueComparationValidator();
		
		validator.validate(null, uiComponentMock, cenaPoRefundacji);
	}
	
	public void validate_PrzepuscJesliCenaRefundacjiPusta() {
		BigDecimal cenaDetaliczna = new BigDecimal(555.0); 
		
		
		Mockito.when(uiComponentMock.getAttributes()).thenReturn(attributesMock);
		Mockito.when(attributesMock.get(ValueComparationValidator.CENA_DETALICZNA_KEY)).thenReturn(cenaDetaliczna);
		
		ValueComparationValidator validator = new ValueComparationValidator();
		
		validator.validate(null, uiComponentMock, null);
	}
	
	public void validate_SprawdzWiadomoscJesliCenaDetalicznaNizszaNizRefundacji() {
		BigDecimal cenaDetaliczna = new BigDecimal(10.0); 
		BigDecimal cenaPoRefundacji = new BigDecimal(100.0); 
		
		
		Mockito.when(uiComponentMock.getAttributes()).thenReturn(attributesMock);
		Mockito.when(attributesMock.get(ValueComparationValidator.CENA_DETALICZNA_KEY)).thenReturn(cenaDetaliczna);
		
		ValueComparationValidator validator = new ValueComparationValidator();
		
		FacesMessage facesMessage = null;
		try {
			validator.validate(null, uiComponentMock, cenaPoRefundacji);
		} catch (ValidatorException e) {
			facesMessage = e.getFacesMessage();
		}
		
		Assert.assertNotNull(facesMessage);
		Assert.assertEquals(facesMessage.getSummary(), ValueComparationValidator.WARTOSC_BLEDNA_MSG);
		Assert.assertEquals(facesMessage.getDetail(), ValueComparationValidator.WARTOSC_BLEDNA_DETAILS);
		Assert.assertEquals(facesMessage.getSeverity(), FacesMessage.SEVERITY_ERROR);
	}

}
